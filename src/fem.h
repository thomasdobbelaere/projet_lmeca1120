
/*
 *  fem.h
 *  Library for MECA1120 : Finite Elements for dummies
 *
 *  Copyright (C) 2018 UCL-IMMC : Vincent Legat
 *  All rights reserved.
 *
 */

#ifndef _FEM_H_
#define _FEM_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define ErrorScan(a)   femErrorScan(a,__LINE__,__FILE__)
#define Error(a)       femError(a,__LINE__,__FILE__)
#define Warning(a)     femWarning(a,  __LINE__, __FILE__)
#define FALSE 0
#define TRUE  1


typedef struct {
    int n;
    double radiusIn;
    double radiusOut;
    double gravity[2];
    double gamma;
    double *x;
    double *y;
    double *vx;
    double *vy;
    double *r;
    double *m;
    double *dvBoundary;
    double *dvContacts;
    int* elem; // element in which the marble is
} femGrains;

typedef struct {
    int iNeigh;
    int nGrain;
    int nodes[6];
    int neighbors[3];
    int *grains;  //grains in the element
} femElement;

typedef struct {
    int *elem;
    femElement *elements;
    double *X;
    double *Y;
    int nElem;
    int nNode;
    int nLocalNode;
    double radiusIn;
    double radiusOut;
} femMesh;

typedef struct {
    int elem[2];
    int node[2];
} femEdge;

typedef struct {
    femMesh *mesh;
    femEdge *edges;
    int nEdge;
    int nBoundary;
} femEdges;

typedef struct {
    int n;
    void (*x2)(double *xsi, double *eta);
    void (*phi2)(double xsi, double eta, double *phi);
    void (*dphi2dx)(double xsi, double eta, double *dphidxsi, double *dphideta);
} femDiscrete;
//
typedef struct {
    int n;
    const double *xsi;
    const double *eta;
    const double *weight;
} femIntegration;

typedef struct
{
    double *R;
    double *D;
    double *S;
    double *X;
    double error;
    int size;
    int iter;
} femIterativeSolver;
//
typedef struct {
    femMesh *mesh;
    femEdges *edges;
    femDiscrete *space;
    femIntegration *rule;
    femIterativeSolver *uSolver;
    femIterativeSolver *vSolver;
    int size;
    // int *number;
    double gamma;
    double vext;
    double mu;
    double *u;
    double *v;
    double *soluce;
} femDiffusionProblem;


femIntegration      *femIntegrationCreate();
void                 femIntegrationFree(femIntegration *theRule);

femMesh             *femMeshRead(const char *filename);
void                 femMeshWrite(const femMesh* myMesh, const char *filename);
void                 femMeshFree(femMesh *theMesh);
void                 femMeshAllocate(femMesh*theMesh, double r);

femEdges*            femEdgesCreate(femMesh *theMesh);
void                 femEdgesFree(femEdges *theEdges);
void                 femEdgesPrint(femEdges *theEdges);
int                  femEdgesCompare(const void *edgeOne, const void *edgeTwo);

femDiscrete*         femDiscreteCreate();
void                 femDiscreteFree(femDiscrete* mySpace);
void                 femDiscretePrint(femDiscrete* mySpace);
void                 femDiscreteXsi2(femDiscrete* mySpace, double *xsi, double *eta);
void                 femDiscretePhi2(femDiscrete* mySpace, double xsi, double eta, double *phi);
void                 femDiscreteDphi2(femDiscrete* mySpace, double xsi, double eta, double *dphidxsi, double *dphideta);

void                 femXsi(femMesh* theMesh, int iElem, double x, double y, double xsi[2]);

femIterativeSolver*  femIterativeSolverCreate(int size);
void                 femIterativeSolverFree(femIterativeSolver* mySolver);
void                 femIterativeSolverInit(femIterativeSolver* mySolver);
void                 femIterativeSolverPrint(femIterativeSolver* mySolver);
void                 femIterativeSolverPrintInfos(femIterativeSolver* mySolver);
double*              femIterativeSolverEliminate(femIterativeSolver* mySolver);
void                 femIterativeSolverConstrain(femIterativeSolver* mySolver, int myNode, double myValue);
void                 femIterativeSolverAssemble(femIterativeSolver* mySolver, double *Aloc, double *Bloc, double *Uloc, int *map, int nLoc);
double               femIterativeSolverGet(femIterativeSolver* mySolver, int i, int j);
int                  femIterativeSolverConverged(femIterativeSolver *mySolver);
//
femDiffusionProblem *femDiffusionCreate(const char *filename, double vext, double gamma, double mu);
void                 femDiffusionFree(femDiffusionProblem *theProblem);
void                 femDiffusionMeshLocal(const femDiffusionProblem *theProblem, const int i, int *map, double *x, double *y, double *u, double *v);
void                 femDiffusionCompute(femDiffusionProblem *theProblem, femGrains *theGrains);

femGrains  *femGrainsCreate(int n, double r, double m, double gamma, double radiusIn, double radiusOut);
void        femGrainsFree(femGrains *myGrains);
void        femGrainsUpdate(femGrains *myGrains, femDiffusionProblem* theProblem, double dt, double tol, double iterMax);
double      femGrainsContactIterate(femGrains *myGrains, double dt, int iter);

int         findElement(double x, double y, femMesh* theMesh, int iStart);
void        femUpdateGrainPosition(femGrains *theGrains, femDiffusionProblem *theProblem);
void        femFluidVelocityAtPoint(femDiffusionProblem *theProblem, int elem, double x, double y, double v[2]);

double      femMin(double *x, int n);
double      femMax(double *x, int n);
void        femError(char *text, int line, char *file);
void        femErrorScan(int test, int line, char *file);
void        femWarning(char *text, int line, char *file);


#endif
