
/*
 *  main.c
 *  Library for MECA1120 : Finite Elements for dummies
 *  Homework 4 for 17-18 : Discrete Grains
 *
 *  Copyright (C) 2018 UCL-IMMC : Vincent Legat
 *  All rights reserved.
 *
 */


#include "glfem.h"
#include <math.h>
#include <getopt.h>


void glfemDrawHeart(double rh) {
	double xh[10] = {  0, -rh,  -rh, -2*rh/3, -rh/3,    0, rh/3, 2*rh/3,   rh, rh};
	double yh[10] = {-rh,   0, rh/2,      rh,    rh, rh/2,   rh,     rh, rh/2,  0};
	glBegin(GL_LINE_STRIP);
	for (int i=0; i < 10; ++i){
		glVertex2f(xh[i],yh[i]);
	}
	glVertex2f(xh[0],yh[0]);
	glEnd();
}

void display_usage(){
	puts(
	"Utilisation: \n"
    "       ./myFem [OPTIONS]...\n"
	"Options et arguments:\n"
	"  -h                    :  affiche cette aide\n"
	"  -n  nombre_de_grains  :  entier donnant le nombre de grains de la simulation. 50 par défaut \n"
	"  -r  rayon_des_grains  :  réel donnant le rayon des grains. 0.05 par défaut \n"
	"  -v  v_ext             :  réel donnant la vitesse de la paroi exterieure. 3. par défaut \n"
	"  -g  gamma             :  réel donnant le coefficient de trainee. .5 par défaut	\n"
	"  -m  mu                :  réel donnant le coefficient de friction du fluide. 10. par défaut \n"
	"  -i  iterMax           :  entier donnant le nombre max d'itérations du solveur  de contact \n"
	);
}

int main(int argc, char const *argv[]) {
    int    n = 50;
    double radius  = .05;
    double mass    = 0.1;
    double dt      = .1;
    double tEnd    = 8.0;
    double tol     = 1e-6;
    double t       = 0;
    int iterMax = 200;
    double gamma   = .5;
    double vext    = 3.;
    double mu      = 10.;

    int opt;
    int flag = 0;
    while((opt = getopt(argc, (char**)argv, "hn:r:v:g:m:i:")) != EOF){
		switch(opt) {
            case 'h':
                flag = 1;
                break;
			case 'n':
                sscanf(optarg, "%d", &n);
				break;
            case 'r':
                sscanf(optarg, "%lf", &radius);
                break;
            case 'v':
                sscanf(optarg, "%lf", &vext);
                break;
            case 'g':
                sscanf(optarg, "%lf", &gamma);
                break;
            case 'm':
                sscanf(optarg, "%lf", &mu);
                break;
			case 'i':
				sscanf(optarg, "%d", &iterMax);
				break;
		}
	}

    if(flag) {
        display_usage();
        exit(EXIT_SUCCESS);
    }

    double R;

    printf("n=%d,r=%f,vext=%f,gamma=%f,mu=%f  \n", n, radius, vext, gamma, mu);

    char meshFileName[] = "../data/meca1120-projet-meshBig.txt";
    femDiffusionProblem* theProblem = femDiffusionCreate(meshFileName, vext, gamma, mu);
	femMeshAllocate(theProblem->mesh, radius);
    double radiusIn  = theProblem->mesh->radiusIn;
    double radiusOut = theProblem->mesh->radiusOut;
    femGrains* theGrains = femGrainsCreate(n ,radius, mass, gamma, radiusIn, radiusOut);
    femUpdateGrainPosition(theGrains, theProblem);


    GLFWwindow* window = glfemInit("MECA1120 : Project");
    glfwMakeContextCurrent(window);
    int theRunningMode = 1.0;
    float theVelocityFactor = 0.25;
	double rh;
    do {
        int i,w,h,testConvergenceU,testConvergenceV;
        double currentTime = glfwGetTime();

        glfwGetFramebufferSize(window,&w,&h);
        glfemReshapeWindows(radiusOut,w,h);
        // glColor3f(0,0,0); glfemPlotMesh(theMesh);
        glfemPlotField(theProblem->mesh, theProblem->soluce);
        for (i=0 ;i < theGrains->n; i++) {
			if(i%7 == 0){
				glColor3f(1,0,0);
			}
			else if(i%7 == 1){
				glColor3f(1,127./255,0);
			}
			else if(i%7 == 2){
				glColor3f(1,1,0);
			}
			else if(i%7 == 3){
				glColor3f(0,1,0);
			}
			else if(i%7 == 4){
				glColor3f(0,0,1);
			}
			else if(i%7 == 5){
				glColor3f(248./255,24./255,148./255.);
			}
			else{
				glColor3f(143./255,0,1);
			}
            glfemDrawDisk(theGrains->x[i],theGrains->y[i],theGrains->r[i]);
			glColor3f(0,0,0);
			glfemDrawCircle(theGrains->x[i], theGrains->y[i], theGrains->r[i]);
        }
		glColor3f(1,0,0); glfemDrawHeart(.1*radiusIn);
		glColor3f(1,127./255,0); glfemDrawHeart(.2*radiusIn);
		glColor3f(1,1,0); glfemDrawHeart(.3*radiusIn);
		glColor3f(0,1,0); glfemDrawHeart(.4*radiusIn);
		glColor3f(0,0,1); glfemDrawHeart(.5*radiusIn);
		glColor3f(248./255,24./255,148./255.); glfemDrawHeart(.6*radiusIn);
		glColor3f(143./255,0,1); glfemDrawHeart(.7*radiusIn);

        glColor3f(1,0,0); glfemDrawCircle(0,0,radiusOut);
        glColor3f(0,0,1); glfemDrawCircle(0,0,radiusIn);
        char theMessage[256];
        sprintf(theMessage,"Time = %g sec",t);
        glColor3f(1,0,0); glfemDrawMessage(20,460,theMessage);
        glfwSwapBuffers(window);
        glfwPollEvents();

        if (t < tEnd && theRunningMode == 1) {
            printf("Time = %4g : ",t);
            femIterativeSolverInit(theProblem->uSolver);
            femIterativeSolverInit(theProblem->vSolver);
            do {
                femDiffusionCompute(theProblem, theGrains);
                // femIterativeSolverPrintInfos(theProblem->uSolver);
                // femIterativeSolverPrintInfos(theProblem->vSolver);
                testConvergenceU = femIterativeSolverConverged(theProblem->uSolver);
                testConvergenceV = femIterativeSolverConverged(theProblem->vSolver);

            } while ( testConvergenceU == 0 || testConvergenceV == 0);
            for (i=0; i < theProblem->size; ++i) {
                theProblem->soluce[i] = sqrt(theProblem->u[i]*theProblem->u[i] + theProblem->v[i]*theProblem->v[i]);
            }
            // printf("press CR to compute the next time step >>");
            // char c= getchar();
            femGrainsUpdate(theGrains, theProblem, dt, tol, iterMax);
            printf("Conj. Grad. : iterations = %4d : error = %14.7e\n", theProblem->uSolver->iter, theProblem->uSolver->error);
            printf("----------\n");
            t += dt;
        }

        while ( glfwGetTime()-currentTime < theVelocityFactor ) {
            if (glfwGetKey(window,'R') == GLFW_PRESS)
        	    theRunningMode = 1;
          if (glfwGetKey(window,'S') == GLFW_PRESS)
        	    theRunningMode = 0;
        }
    } while (glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && (!glfwWindowShouldClose(window)));


    glfwTerminate();
    femGrainsFree(theGrains);
    femDiffusionFree(theProblem);
    exit(EXIT_SUCCESS);
}
