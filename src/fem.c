
/*
 *  fem.c
 *  Library for MECA1120 : Finite Elements for dummies
 *
 *  Copyright (C) 2018 UCL-IMMC : Vincent Legat
 *  All rights reserved.
 *
 */
 static const double _gaussTri3Xsi[3]     = { 0.166666666666667, 0.666666666666667, 0.166666666666667};
 static const double _gaussTri3Eta[3]     = { 0.166666666666667, 0.166666666666667, 0.666666666666667};
 static const double _gaussTri3Weight[3]  = { 0.166666666666667, 0.166666666666667, 0.166666666666667};

#include "fem.h"

femIntegration *femIntegrationCreate()
{
    femIntegration *theRule = malloc(sizeof(femIntegration));
    theRule->n      = 3;
    theRule->xsi    = _gaussTri3Xsi;
    theRule->eta    = _gaussTri3Eta;
    theRule->weight = _gaussTri3Weight;
    return theRule;
}

void femIntegrationFree(femIntegration *theRule)
{
    free(theRule);
}

void _p1c0_x(double *xsi, double *eta)
{
    xsi[0] =  0.0;  eta[0] =  0.0;
    xsi[1] =  1.0;  eta[1] =  0.0;
    xsi[2] =  0.0;  eta[2] =  1.0;
}

void _p1c0_phi(double xsi, double eta, double *phi)
{
    phi[0] = 1 - xsi - eta;
    phi[1] = xsi;
    phi[2] = eta;
}

void _p1c0_dphidx(double xsi, double eta, double *dphidxsi, double *dphideta)
{
    dphidxsi[0] = -1.0;
    dphidxsi[1] =  1.0;
    dphidxsi[2] =  0.0;
    dphideta[0] = -1.0;
    dphideta[1] =  0.0;
    dphideta[2] =  1.0;

}


femDiscrete *femDiscreteCreate() {
    femDiscrete *theSpace = malloc(sizeof(femDiscrete));
    theSpace->n       = 3;
    theSpace->x2      = _p1c0_x;
    theSpace->phi2    = _p1c0_phi;
    theSpace->dphi2dx = _p1c0_dphidx;
    return theSpace;
}

void femDiscreteFree(femDiscrete *theSpace)
{
    free(theSpace);
}

void femDiscreteXsi2(femDiscrete* mySpace, double *xsi, double *eta)
{
    mySpace->x2(xsi,eta);
}

void femDiscretePhi2(femDiscrete* mySpace, double xsi, double eta, double *phi)
{
    mySpace->phi2(xsi,eta,phi);
}

void femDiscreteDphi2(femDiscrete* mySpace, double xsi, double eta, double *dphidxsi, double *dphideta)
{
    mySpace->dphi2dx(xsi,eta,dphidxsi,dphideta);
}

void femDiscretePrint(femDiscrete *mySpace)
{
    int i,j;
    int n = mySpace->n;
    double xsi[4], eta[4], phi[4], dphidxsi[4], dphideta[4];

    femDiscreteXsi2(mySpace,xsi,eta);
    for (i=0; i < n; i++) {

        femDiscretePhi2(mySpace,xsi[i],eta[i],phi);
        femDiscreteDphi2(mySpace,xsi[i],eta[i],dphidxsi,dphideta);

        for (j=0; j < n; j++)  {
            printf("(xsi=%+.1f,eta=%+.1f) : ",xsi[i],eta[i]);
            printf(" phi(%d)=%+.1f",j,phi[j]);
            printf("   dphidxsi(%d)=%+.1f",j,dphidxsi[j]);
            printf("   dphideta(%d)=%+.1f \n",j,dphideta[j]);  }
        printf(" \n"); }
}



femMesh *femMeshRead(const char *filename)
{
    femMesh *theMesh = malloc(sizeof(femMesh));
    double R;
    double area;
    double radiusIn = 1000.;
    double radiusOut = 0.;
    int i,j,trash,*elem;

    FILE* file = fopen(filename,"r");
    if (file == NULL) Error("No mesh file !");

    ErrorScan(fscanf(file, "Number of nodes %d \n", &theMesh->nNode));
    theMesh->X = malloc(sizeof(double)*theMesh->nNode);
    theMesh->Y = malloc(sizeof(double)*theMesh->nNode);
    for (i = 0; i < theMesh->nNode; ++i) {
        ErrorScan(fscanf(file,"%d : %le %le \n",&trash,&theMesh->X[i],&theMesh->Y[i]));
        R = sqrt(theMesh->X[i]*theMesh->X[i] + theMesh->Y[i]*theMesh->Y[i]);
        radiusIn  = fmin(radiusIn, R);
        radiusOut = fmax(radiusOut, R);
    }
    printf("%f\n", radiusIn);
    printf("%f\n", radiusOut);
    theMesh->radiusIn  = radiusIn;
    theMesh->radiusOut = radiusOut;

    char str[256]; if (fgets(str, sizeof(str), file) == NULL) Error("Corrupted mesh file !");

    if (!strncmp(str,"Number of triangles",19))  {
        ErrorScan(sscanf(str,"Number of triangles %d \n", &theMesh->nElem));
        theMesh->elem = malloc(sizeof(int)*3*theMesh->nElem);
        femElement *elements = malloc(sizeof(femElement)*theMesh->nElem);
        // femElement *el;
        theMesh->elements = elements;
        theMesh->nLocalNode = 3;
        for (i = 0; i < theMesh->nElem; ++i) {
            elem = &(theMesh->elem[i*3]);
            elements[i].iNeigh = 0;
            elements[i].nGrain = 0;
            elements[i].grains = malloc(5*sizeof(int));
            for (j = 0; j < 3; ++j){
                elements[i].neighbors[j] = -1;
                elements[i].nodes[2*j] = -1;
                elements[i].nodes[2*j+1] = -1;
            }
            ErrorScan(fscanf(file,"%d : %d %d %d\n", &trash,&elem[0],&elem[1],&elem[2]));
        }
    }
    fclose(file);
    return theMesh;
}

void femMeshFree(femMesh *theMesh) {
    int i;
    free(theMesh->X);
    free(theMesh->Y);
    free(theMesh->elem);
    for(i =  0; i < theMesh->nElem; ++i){
        free(theMesh->elements[i].grains);
    }
    free(theMesh->elements);
    free(theMesh);
}

void femMeshWrite(const femMesh *theMesh, const char *filename)
{
    int i,*elem;

    FILE* file = fopen(filename,"w");

    fprintf(file, "Number of nodes %d \n", theMesh->nNode);
    for (i = 0; i < theMesh->nNode; ++i) {
        fprintf(file,"%6d : %14.7e %14.7e \n",i,theMesh->X[i],theMesh->Y[i]);
    }
    if (theMesh->nLocalNode == 3) {
        fprintf(file, "Number of triangles %d \n", theMesh->nElem);
        for (i = 0; i < theMesh->nElem; ++i) {
            elem = &(theMesh->elem[i*3]);
            fprintf(file,"%6d : %6d %6d %6d \n", i,elem[0],elem[1],elem[2]);
        }
    }
    fclose(file);
}

femEdges *femEdgesCreate(femMesh *theMesh) {
    femEdges *theEdges = malloc(sizeof(femEdges));
    femElement *e;
    int nLoc = theMesh->nLocalNode;
    int i,j,n = theMesh->nElem * nLoc;
    femEdge* edges = malloc(n * sizeof(femEdge));
    theEdges->mesh  = theMesh;
    theEdges->edges = edges;
    theEdges->nEdge = n;
    theEdges->nBoundary = n;

    for (i = 0; i < theMesh->nElem; i++) {
        int *elem = &(theMesh->elem[i*nLoc]);
        for (j = 0; j < nLoc; j++) {
            int id = i * nLoc + j;
            edges[id].elem[0] = i;
            edges[id].elem[1] = -1;
            edges[id].node[0] = elem[j];
            edges[id].node[1] = elem[(j + 1) % nLoc];
        }
    }

    qsort(theEdges->edges, theEdges->nEdge, sizeof(femEdge), femEdgesCompare);

    int index = 0;
    int nBoundary = 0;

    for (i=0; i < theEdges->nEdge; i++) {
        if (i == theEdges->nEdge - 1 || femEdgesCompare(&edges[i],&edges[i+1]) != 0) {
            edges[index] = edges[i];
            // -1 if on the inner boundary
            // -2 if on the outer boundary
            j = edges[index].node[0];
            if (sqrt(theMesh->X[j]*theMesh->X[j] + theMesh->Y[j]*theMesh->Y[j]) >= theMesh->radiusOut) {
                edges[index].elem[1] = -2;
            }

            nBoundary++;
        }
        else {
            edges[index] = edges[i];
            edges[index].elem[1] = edges[i+1].elem[0];
            e = &(theMesh->elements[edges[index].elem[1]]);
            j = e->iNeigh;
            e->neighbors[j] = edges[index].elem[0];
            e->nodes[2*j] = edges[index].node[1];
            e->nodes[2*j+1] = edges[index].node[0];
            e->iNeigh += 1;
            i = i+1;
        }
        e = &(theMesh->elements[edges[index].elem[0]]);
        j = e->iNeigh;
        e->neighbors[j] = edges[index].elem[1];
        e->nodes[2*j] = edges[index].node[0];
        e->nodes[2*j+1] = edges[index].node[1];
        e->iNeigh += 1;
        index++;
    }
    theEdges->edges = realloc(edges, index * sizeof(femEdge));
    theEdges->nEdge = index;
    theEdges->nBoundary = nBoundary;
    return theEdges;
}

void femEdgesPrint(femEdges *theEdges)
{
    int i;
    for (i = 0; i < theEdges->nEdge; ++i) {
        printf("%6d : %4d %4d : %4d %4d \n",i,
               theEdges->edges[i].node[0],theEdges->edges[i].node[1],
               theEdges->edges[i].elem[0],theEdges->edges[i].elem[1]); }
}

void femEdgesFree(femEdges *theEdges)
{
    free(theEdges->edges);
    free(theEdges);
}

int femEdgesCompare(const void *edgeOne, const void *edgeTwo)
{
    int *nodeOne = ((femEdge*) edgeOne)->node;
    int *nodeTwo = ((femEdge*) edgeTwo)->node;
    int  diffMin = fmin(nodeOne[0],nodeOne[1]) - fmin(nodeTwo[0],nodeTwo[1]);
    int  diffMax = fmax(nodeOne[0],nodeOne[1]) - fmax(nodeTwo[0],nodeTwo[1]);

    if (diffMin < 0)    return  1;
    if (diffMin > 0)    return -1;
    if (diffMax < 0)    return  1;
    if (diffMax > 0)    return -1;
                        return  0;
}

femIterativeSolver *femIterativeSolverCreate(int size)
{
    femIterativeSolver *mySolver = malloc(sizeof(femIterativeSolver));
    mySolver->R = malloc(sizeof(double)*size*4);
    mySolver->D = mySolver->R + size;
    mySolver->S = mySolver->R + size*2;
    mySolver->X = mySolver->R + size*3;
    mySolver->size = size;
    femIterativeSolverInit(mySolver);
    return(mySolver);
}
//
void femIterativeSolverFree(femIterativeSolver *mySolver)
{
    free(mySolver->R);
    free(mySolver);
}
//
void femIterativeSolverInit(femIterativeSolver *mySolver)
{
    int i;
    mySolver->iter = 0;
    mySolver->error = 10.0e+12;
    for (i=0 ; i < mySolver->size*4 ; i++)
        mySolver->R[i] = 0;
}
//
void femIterativeSolverPrint(femIterativeSolver *mySolver)
{
    double  *R;
    int     i, size;
    R    = mySolver->R;
    size = mySolver->size;

    for (i=0; i < size; i++) {
        printf("%d :  %+.1e \n",i,R[i]); }
}

void femIterativeSolverPrintInfos(femIterativeSolver *mySolver)
{
    if (mySolver->iter == 1)     printf("\n    Iterative solver \n");
    printf("    Iteration %4d : %14.7e\n",mySolver->iter,mySolver->error);
}

int femIterativeSolverConverged(femIterativeSolver *mySolver)
{
    int  testConvergence = 0;
    if (mySolver->iter  > 3000)     testConvergence = -1;
    if (mySolver->error < 10.0e-6)  testConvergence = 1;
    return(testConvergence);
}
//
femDiffusionProblem *femDiffusionCreate(const char *filename, double vext, double gamma, double mu) {
    int i;
    double x,y,r,V;
    femDiffusionProblem *theProblem = malloc(sizeof(femDiffusionProblem));
    theProblem->mesh  = femMeshRead(filename);
    double rIn  = theProblem->mesh->radiusIn;
    double rOut = theProblem->mesh->radiusOut;
    theProblem->edges = femEdgesCreate(theProblem->mesh);
    theProblem->space = femDiscreteCreate();
    theProblem->rule  = femIntegrationCreate();
    theProblem->size  = theProblem->mesh->nNode;
    theProblem->uSolver = femIterativeSolverCreate(theProblem->size);
    theProblem->vSolver = femIterativeSolverCreate(theProblem->size);
    theProblem->vext = vext;
    theProblem->gamma = gamma;
    theProblem->mu = mu;
    theProblem->u = malloc(sizeof(double)*theProblem->size);
    theProblem->v = malloc(sizeof(double)*theProblem->size);
    theProblem->soluce = malloc(sizeof(double)*theProblem->size);
    for (i = 0; i < theProblem->size; i++){
        x = theProblem->mesh->X[i];
        y = theProblem->mesh->Y[i];
        r = sqrt(x*x + y*y);
        V = (r-rIn)/(rOut-rIn)*vext;
        theProblem->u[i] =  y/r*V;
        theProblem->v[i] = -x/r*V;
        theProblem->soluce[i] = sqrt(theProblem->u[i]*theProblem->u[i]+theProblem->v[i]*theProblem->v[i]);
    }
    return theProblem;
}
//
void femDiffusionFree(femDiffusionProblem *theProblem)
{
    femIntegrationFree(theProblem->rule);
    femDiscreteFree(theProblem->space);
    femEdgesFree(theProblem->edges);
    femMeshFree(theProblem->mesh);
    femIterativeSolverFree(theProblem->uSolver);
    femIterativeSolverFree(theProblem->vSolver);
    free(theProblem->u);
    free(theProblem->v);
    free(theProblem->soluce);
    free(theProblem);
}
//
//
void femDiffusionMeshLocal(const femDiffusionProblem *theProblem, const int iElem, int *map, double *x, double *y, double *u, double *v) {
    femMesh *theMesh = theProblem->mesh;
    int j,nLocal = theMesh->nLocalNode;

    for (j=0; j < nLocal; ++j) {
        map[j] = theMesh->elem[iElem*nLocal+j];
        x[j]   = theMesh->X[map[j]];
        y[j]   = theMesh->Y[map[j]];
        u[j]   = theProblem->u[map[j]];
        v[j]   = theProblem->v[map[j]];
    }
}
//
void femDiffusionCompute(femDiffusionProblem *theProblem, femGrains *theGrains)
{
    femMesh *theMesh = theProblem->mesh;
    femIntegration *theRule = theProblem->rule;
    femDiscrete *theSpace = theProblem->space;
    femIterativeSolver *uSolver = theProblem->uSolver;
    femIterativeSolver *vSolver = theProblem->vSolver;
    femEdges *theEdges = theProblem->edges;
    double *x    = theGrains->x;
    double *y    = theGrains->y;
    double *vx   = theGrains->vx;
    double *vy   = theGrains->vy;
    double vext  = theProblem->vext;
    double gamma = theProblem->gamma;
    double mu    = theProblem->mu;
    // int *number = theProblem->number;

    if (theSpace->n > 4) Error("Unexpected discrete space size !");

    double Xloc[4],Yloc[4],phi[4],dphidxsi[4],dphideta[4],dphidx[4],dphidy[4],
    Aloc[16],Buloc[4],Bvloc[4],Uloc[4],Vloc[4];
    int iEdge,iElem,iInteg,i,j,map[4],k,iGrain;
    double xsiGrain[2];

    for (iElem = 0; iElem < theMesh->nElem; iElem++) {
        for (i = 0; i < theSpace->n; i++){
             Buloc[i] = 0;
             Bvloc[i] = 0;
        }
        for (i = 0; i < (theSpace->n)*(theSpace->n); i++){
             Aloc[i] = 0;
        }
        femDiffusionMeshLocal(theProblem, iElem, map, Xloc, Yloc, Uloc, Vloc);
        for (iInteg=0; iInteg < theRule->n; iInteg++) {
            double xsi    = theRule->xsi[iInteg];
            double eta    = theRule->eta[iInteg];
            double weight = theRule->weight[iInteg];
            femDiscretePhi2(theSpace,xsi,eta,phi);
            femDiscreteDphi2(theSpace,xsi,eta,dphidxsi,dphideta);
            double dxdxsi = 0;
            double dxdeta = 0;
            double dydxsi = 0;
            double dydeta = 0;
            for (i = 0; i < theSpace->n; i++) {
                dxdxsi += Xloc[i]*dphidxsi[i];
                dxdeta += Xloc[i]*dphideta[i];
                dydxsi += Yloc[i]*dphidxsi[i];
                dydeta += Yloc[i]*dphideta[i];
            }
            double jac = fabs(dxdxsi * dydeta - dxdeta * dydxsi);
            for (i = 0; i < theSpace->n; i++) {
                dphidx[i] = (dphidxsi[i] * dydeta - dphideta[i] * dydxsi) / jac;
                dphidy[i] = (dphideta[i] * dxdxsi - dphidxsi[i] * dxdeta) / jac;
            }
            for (i = 0; i < theSpace->n; i++) {
                for(j = 0; j < theSpace->n; j++) {
                    Aloc[i*(theSpace->n)+j] += mu*(dphidx[i] * dphidx[j]
                                            + dphidy[i] * dphidy[j]) * jac * weight;
                }
            }
            // for (i = 0; i < theSpace->n; i++) {
            //     Bloc[i] += phi[i] * jac *weight;
            // }
        } //fin boucle sur les points d'intégration
        for (i = 0; i < theSpace->n; ++i) {
            for (j = 0; j < theSpace->n; ++j) {
                for (k = 0; k < theMesh->elements[iElem].nGrain; ++k) {
                    iGrain = theMesh->elements[iElem].grains[k];
                    femXsi(theMesh, iElem, x[iGrain], y[iGrain], xsiGrain);
                    femDiscretePhi2(theSpace, xsiGrain[0], xsiGrain[1], phi);
                    Aloc[i*(theSpace->n)+j] += gamma*phi[i]*phi[j];
                }
            }
        }
        for (i = 0; i < theSpace->n; ++i) {
            for (k = 0; k < theMesh->elements[iElem].nGrain; ++k) {
                iGrain = theMesh->elements[iElem].grains[k];
                femXsi(theMesh, iElem, x[iGrain], y[iGrain], xsiGrain);
                femDiscretePhi2(theSpace, xsiGrain[0], xsiGrain[1], phi);
                Buloc[i] += phi[i]*vx[k];
                Bvloc[i] += phi[i]*vy[k];
            }
        }
        femIterativeSolverAssemble(uSolver,Aloc,Buloc,Uloc,map,theSpace->n);
        femIterativeSolverAssemble(vSolver,Aloc,Bvloc,Vloc,map,theSpace->n);
     }
     int node1,node2;
    for (iEdge= 0; iEdge < theEdges->nEdge; iEdge++) {
        if (theEdges->edges[iEdge].elem[1] < 0) {
            femIterativeSolverConstrain(uSolver,theEdges->edges[iEdge].node[0],0.0);
            femIterativeSolverConstrain(uSolver,theEdges->edges[iEdge].node[1],0.0);
            femIterativeSolverConstrain(vSolver,theEdges->edges[iEdge].node[0],0.0);
            femIterativeSolverConstrain(vSolver,theEdges->edges[iEdge].node[1],0.0);
        }
    }
    // u et v sont des deltas
    double *u = femIterativeSolverEliminate(uSolver);
    double *v = femIterativeSolverEliminate(vSolver);
    for (i = 0; i < theProblem->size; ++i) {
        theProblem->u[i] += u[i];
        theProblem->v[i] += v[i];
    }
}

// -----------------------------------------------------------------------------

femGrains *femGrainsCreate(int n, double r, double m, double gamma, double radiusIn, double radiusOut)
{
    int test = 10;
    test += test;
    printf("%d\n", test);
    int i,nContact = n*(n-1)/2;

    femGrains *theGrains = malloc(sizeof(femGrains));
    theGrains->n = n;
    theGrains->radiusIn = radiusIn;
    theGrains->radiusOut = radiusOut;
    theGrains->gravity[0] =  0.0;
    theGrains->gravity[1] = -9.81;
    theGrains->gamma = gamma;


    theGrains->x  = malloc(n*sizeof(double));
    theGrains->y  = malloc(n*sizeof(double));
    theGrains->vx = malloc(n*sizeof(double));
    theGrains->vy = malloc(n*sizeof(double));
    theGrains->r  = malloc(n*sizeof(double));
    theGrains->m  = malloc(n*sizeof(double));
    theGrains->dvBoundary = malloc(n * sizeof(double));
    theGrains->dvContacts = malloc(nContact * sizeof(double));
    theGrains->elem = malloc(n*sizeof(int));

    for(i = 0; i < n; i++) {
        theGrains->r[i] = r;
        theGrains->m[i] = m;
        theGrains->x[i] = (i%5) * r * 2.5 - 5 * r + 1e-8;
        theGrains->y[i] = (i/5) * r * 2.5 + 2 * r + radiusIn;
        theGrains->vx[i] = 0.0;
        theGrains->vy[i] = 0.0;
        theGrains->dvBoundary[i] = 0.0;
        theGrains->elem[i] = -1;
    }

    for(i = 0; i < nContact; i++)
        theGrains->dvContacts[i] = 0.0;


    return theGrains;
}

void femMeshAllocate(femMesh* theMesh, double r) {
    int i,j,iNode,n;
    double area;
    double maxArea = 0;
    double X[3]; double Y[3];
    for (i = 0; i < theMesh->nElem; ++i) {
        for(j=0; j < 3; ++j) {
            iNode = theMesh->elem[3*i+j];
            X[j] = theMesh->X[iNode];
            Y[j] = theMesh->Y[iNode];
        }
        area = (X[1]-X[0])*(Y[2]-Y[0]) - (X[2]-X[0])*(Y[1]-Y[0]);
        maxArea = fmax(maxArea, area);
    }
    n = (int)(maxArea/(3.14*r*r) + 1);
    printf("n=%d\n", n);
    for(i = 0; i < theMesh->nElem; ++i){
        theMesh->elements[i].grains = realloc(theMesh->elements[i].grains, n*sizeof(int));
    }
}

void femGrainsFree(femGrains *theGrains)
{
    free(theGrains->x);
    free(theGrains->y);
    free(theGrains->vx);
    free(theGrains->vy);
    free(theGrains->r);
    free(theGrains->m);
    free(theGrains->dvBoundary);
    free(theGrains->dvContacts);
    free(theGrains->elem);
    free(theGrains);
}

void femXsi(femMesh *theMesh, int iElem, double x, double y, double xsi[2]) {
    int nLoc = theMesh->nLocalNode;
    int iNode;
    double X[nLoc]; double Y[nLoc];
    for(int i = 0; i < nLoc; ++i) {
        iNode = theMesh->elem[nLoc*iElem + i];
        X[i] = theMesh->X[iNode];
        Y[i] = theMesh->Y[iNode];
    }
    xsi[0] = ( (Y[2]-Y[0])*(x-X[0]) + (X[2]-X[0])*(Y[0]-y) )/
             ( (X[1]-X[0])*(Y[2]-Y[0]) - (X[2]-X[0])*(Y[1]-Y[0]) );
    xsi[1] = ( (Y[2]-Y[0])*(x-X[0]) + (X[1]-X[0])*(Y[0]-y) )/
             ( (X[2]-X[0])*(Y[1]-Y[0]) - (X[1]-X[0])*(Y[2]-Y[0]) );
}

int findElement(double x, double y, femMesh *theMesh, int iStart) {
    femElement* theElem = &(theMesh->elements[iStart]);
    int iEdge;
    double area, X1, Y1, X2, Y2;
    double r = sqrt(x*x + y*y);
    if (iStart == -1){
        if (r < theMesh->radiusIn || r > theMesh->radiusOut) {
            return -1;
        }
        else {
            return findElement(x, y, theMesh, 0);
        }
    }
    else{
        for (iEdge = 0; iEdge < 3; ++iEdge) {
            X1 = theMesh->X[theElem->nodes[2*iEdge]];
            Y1 = theMesh->Y[theElem->nodes[2*iEdge]];
            X2 = theMesh->X[theElem->nodes[2*iEdge+1]];
            Y2 = theMesh->Y[theElem->nodes[2*iEdge+1]];
            area = (X2-X1)*(y-Y1) - (x-X1)*(Y2-Y1);
            if (area < 0.0 && theElem->neighbors[iEdge] >= 0) {
                return findElement(x, y, theMesh, theElem->neighbors[iEdge]);
            }
        }
    }
    return iStart;
}

void femFluidVelocityAtPoint(femDiffusionProblem *theProblem, int el, double x, double y, double uv[2]) {
    if(el > -1) {
        double U; double V;
        double* phi = malloc(3*sizeof(double));
        double xsiGrain[2];
        femXsi(theProblem->mesh, el, x, y, xsiGrain);
        femDiscretePhi2(theProblem->space, xsiGrain[0], xsiGrain[1], phi);
        int i, iNode;
        for (i = 0; i < 3; ++i) {
            iNode = theProblem->mesh->elem[3*el+i];
            U = theProblem->u[iNode];
            V = theProblem->v[iNode];
            uv[0] += U*phi[i];
            uv[1] += V*phi[i];
        }
        free(phi);
    }
}

void femUpdateGrainPosition(femGrains *theGrains, femDiffusionProblem *theProblem) {
    int i,iElem,j;
    int n = theGrains->n;
    int nElem = theProblem->mesh->nElem;
    // on remet les compteurs à zero
    for (i = 0; i < nElem; ++i) {
        theProblem->mesh->elements[i].nGrain = 0;
    }
    // on cherche les positions des grains
    for (i = 0; i < n; ++i) {
        iElem = findElement(theGrains->x[i], theGrains->y[i], theProblem->mesh, theGrains->elem[i]);
        theGrains->elem[i] = iElem;
        if(iElem > -1) {
            j = theProblem->mesh->elements[iElem].nGrain;
            theProblem->mesh->elements[iElem].grains[j] = i;
            theProblem->mesh->elements[iElem].nGrain += 1;
        }
    }
}

double femGrainsContactIterate(femGrains *myGrains, double dt, int iter)
{
    int n = myGrains->n;
    double *x          = myGrains->x;
    double *y          = myGrains->y;
    double *m          = myGrains->m;
    double *r          = myGrains->r;
    double *vy         = myGrains->vy;
    double *vx         = myGrains->vx;
    double *dvBoundary = myGrains->dvBoundary;
    double *dvContacts = myGrains->dvContacts;
    double rIn         = myGrains->radiusIn;
    double rOut        = myGrains->radiusOut;

    double zeta = 0.0;
    double rij, ri, gammaij, nx, ny, vn, dv, dvIn, dvOut;
    int iContacts = 0;

    if(iter == 1){
        for (int i = 0; i < n-1; ++i) {
            dvBoundary[i] = 0.;
            for (int j = i+1; j < n; ++j) {
                dvContacts[iContacts] = 0.;
                iContacts += 1;
            }
        }

        dvBoundary[n-1] = 0.;
    iContacts = 0;
    }

    for (int i = 0; i < n;++i) {
        // boundaries
        ri = sqrt(x[i]*x[i] + y[i]*y[i]);
        nx = x[i]/ri;
        ny = y[i]/ri;
        vn = vx[i]*nx + vy[i]*ny;
        dvIn  = fmax(0, -vn-dvBoundary[i]-(ri-rIn-r[i])/dt);
        dvOut = fmax(0, vn + dvBoundary[i] - (rOut-ri-r[i])/dt);
        dv = dvOut-dvIn-dvBoundary[i];
        vx[i] -= dv*nx;
        vy[i] -= dv*ny;
        dvBoundary[i] += dv;
        zeta = fmax(zeta, fabs(dv));
    }

    for(int i = 0; i < n-1; ++i) {
        for (int j = i+1; j < n; ++j) {
            // contacts
            rij   = sqrt( pow(x[j]-x[i], 2) + pow(y[j]-y[i], 2) );
            nx    = (x[j]-x[i])/rij;
            ny    = (y[j]-y[i])/rij;
            gammaij = rij - r[i] - r[j];
            vn    = (vx[i]-vx[j])*nx + (vy[i]-vy[j])*ny;
            dv    = fmax(0, vn + dvContacts[iContacts] - gammaij/dt) - dvContacts[iContacts];

            vx[i] -= dv*nx*m[j]/(m[i] + m[j]);
            vy[i] -= dv*ny*m[j]/(m[i] + m[j]);
            vx[j] += dv*nx*m[i]/(m[i] + m[j]);
            vy[j] += dv*ny*m[i]/(m[i] + m[j]);
            dvContacts[iContacts] += dv;
            zeta = fmax(zeta, fabs(dv));

            iContacts += 1;
        }
    }

    return zeta;

}

void femGrainsUpdate(femGrains *myGrains, femDiffusionProblem *myProblem, double dt, double tol, double iterMax)
{
    int n = myGrains->n;
    int i,iter = 1;
    double zeta;
    double *x          = myGrains->x;
    double *y          = myGrains->y;
    double *m          = myGrains->m;
    double *vy         = myGrains->vy;
    double *vx         = myGrains->vx;
    int *elem          = myGrains->elem;
    double gamma       = myGrains->gamma;
    double uv[2];
    double vmax = 0;

    for (int i = 0; i < n; ++i) {
        uv[0] = 0; uv[1] = 0;
        femFluidVelocityAtPoint(myProblem, elem[i], x[i], y[i], uv);
        vx[i] += -dt/m[i]*gamma*(vx[i]-uv[0]);
        vy[i] += -dt/m[i]*(m[i]*9.81 + gamma*(vy[i]-uv[1]));
    }

    do {
        zeta = femGrainsContactIterate(myGrains,dt,iter);
        iter++;
    } while ((zeta > tol/dt && iter < iterMax) || iter == 1);
    printf("iterations = %4d : error = %14.7e \n",iter-1,zeta);

    for (i = 0; i < n; ++i) {
        x[i] += vx[i] * dt;
        y[i] += vy[i] * dt;
    }
    femUpdateGrainPosition(myGrains, myProblem);
}

void femIterativeSolverAssemble(femIterativeSolver* mySolver, double *Aloc, double *Bloc, double *Uloc, int *map, int nLoc)
{
    int i,j;
    if(mySolver->iter == 0){
        for (i = 0; i < nLoc; i++) {
            int myRow = map[i];
            for(j = 0; j < nLoc; j++) {
                mySolver->R[myRow] += Aloc[i*nLoc+j]*Uloc[j];
            }
            mySolver->R[myRow] -= Bloc[i];
        }
    }

    for (i = 0; i < nLoc; i++) {
        int myRow = map[i];
        for(j = 0; j < nLoc; j++) {
            mySolver->S[myRow] += Aloc[i*nLoc+j]*(mySolver->D[map[j]]);
        }
    }
}
//
void femIterativeSolverConstrain(femIterativeSolver* mySolver, int myNode, double myValue)
{
    mySolver->S[myNode] = 0.;
    mySolver->R[myNode] = 0.;
}
//
double *femIterativeSolverEliminate(femIterativeSolver *mySolver)
{
    mySolver->iter++;
    double error  = 0.0; int i;
    for (i=0; i < mySolver->size; i++) {
        error  += (mySolver->R[i])*(mySolver->R[i]);
    }
    if (mySolver->iter == 1) {
        for (int i = 0; i < mySolver->size; ++i) {
            mySolver->X[i] = 0;
            mySolver->D[i] = mySolver->R[i];
        }
    }
    else{
        double alphaD = 0.0;
        double betaN  = 0.0;
        for (int i = 0; i < mySolver->size; ++i) {
            alphaD += (mySolver->R[i])*(mySolver->S[i]);
        }
        double alpha = -error/alphaD;
        for (i=0; i < mySolver->size; i++) {
            mySolver->X[i] = alpha*(mySolver->D[i]);
            mySolver->R[i] += alpha*(mySolver->S[i]);
            betaN += (mySolver->R[i])*(mySolver->R[i]);
        }
        double beta = betaN/error;
        for (i=0; i < mySolver->size; ++i) {
            mySolver->D[i] = mySolver->R[i] + beta*(mySolver->D[i]);
            mySolver->S[i] = 0.0;
        }
    }

    mySolver->error = sqrt(error);
    return(mySolver->X);
}


double femMin(double *x, int n)
{
    double myMin = x[0];
    int i;
    for (i=1 ;i < n; i++)
        myMin = fmin(myMin,x[i]);
    return myMin;
}

double femMax(double *x, int n)
{
    double myMax = x[0];
    int i;
    for (i=1 ;i < n; i++)
        myMax = fmax(myMax,x[i]);
    return myMax;
}

void femError(char *text, int line, char *file)
{
    printf("\n-------------------------------------------------------------------------------- ");
    printf("\n  Error in %s at line %d : \n  %s\n", file, line, text);
    printf("--------------------------------------------------------------------- Yek Yek !! \n\n");
    exit(69);
}

void femErrorScan(int test, int line, char *file)
{
    if (test >= 0)  return;

    printf("\n-------------------------------------------------------------------------------- ");
    printf("\n  Error in fscanf or fgets in %s at line %d : \n", file, line);
    printf("--------------------------------------------------------------------- Yek Yek !! \n\n");
    exit(69);
}

void femWarning(char *text, int line, char *file)
{
    printf("\n-------------------------------------------------------------------------------- ");
    printf("\n  Warning in %s at line %d : \n  %s\n", file, line, text);
    printf("--------------------------------------------------------------------- Yek Yek !! \n\n");
}
